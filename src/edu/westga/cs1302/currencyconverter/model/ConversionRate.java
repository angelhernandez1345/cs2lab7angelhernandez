package edu.westga.cs1302.currencyconverter.model;

/**
 * Enum to store conversion rates.
 * 
 * @author CS1302
 * @version Fall 2021
 */
public enum ConversionRate {

	AUD(1.36806), 
	CAD(1.24679), 
	CNY(6.44281), 
	EUR(0.86368), 
	GBP(0.73414), 
	INR(74.8500), 
	JPY(112.21);

	private final double conversionFactor;

	/**
	 * Creates conversion rate with the specified factor.
	 * 
	 * @param factor the conversion factor
	 */
	ConversionRate(double factor) {
		this.conversionFactor = factor;
	}

	/**
	 * Returns the amount exchanged to this currency
	 * 
	 * @precondition none
	 * @postcondition none 
	 * @param amount the specified amount
	 * @return the exchanged amount
	 */
	public double exchange(double amount) {
		return this.conversionFactor * amount;
	}
}

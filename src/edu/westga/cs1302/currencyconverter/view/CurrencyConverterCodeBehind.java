package edu.westga.cs1302.currencyconverter.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * The Class CurrencyConverterCodeBehind.
 * 
 * @author Angel Hernandez
 * @version 10/30/2021
 *
 */
public class CurrencyConverterCodeBehind {
	   @FXML
	    private Label errorMessageLabel;

	    @FXML
	    private TextField amountTextField;

	    @FXML
	    private TextField targetCurrencyTextField;

	    @FXML
	    private Button convertButton;

	    @FXML
	    private TextArea exchangedAmountTextArea;
	    
	    private int  amount;
	    private String currency;
	
	/**
	 * Instantiates a new currency converter code behind.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public CurrencyConverterCodeBehind() {
		this.amount = 0;
		this.currency = "";
	}

	@FXML
	 void initialize() {
		this.exchangedAmountTextArea.setEditable(false);
	}
	
	@FXML
	 void handleConvertAmount(ActionEvent event) {
		this.amount = Integer.parseInt(this.amountTextField.getText());
		this.currency = this.targetCurrencyTextField.getText();
	}
}

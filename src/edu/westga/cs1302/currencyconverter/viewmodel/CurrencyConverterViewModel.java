package edu.westga.cs1302.currencyconverter.viewmodel;

import edu.westga.cs1302.currencyconverter.model.ConversionRate;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * The Class CurrencyConverterViewModel.
 * 
 * @author Angel Hernandez
 * @version 10/30/2021
 */
public class CurrencyConverterViewModel {
	private ConversionRate theRate;
	
	private DoubleProperty amountProperty;
	private StringProperty targetCurrencyProperty;
	private StringProperty resultProperty;
	private StringProperty errorMessageProperty;

	/**
	 * Instantiates a new currency converter view model.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public CurrencyConverterViewModel() {
		this.amountProperty = new SimpleDoubleProperty();
		this.targetCurrencyProperty = new SimpleStringProperty();
		this.resultProperty = new SimpleStringProperty();
		this.errorMessageProperty = new SimpleStringProperty();
	}
	
	

	/**
	 * Gets the amount property.
	 * 
	 * @return the amountProperty
	 */
	public DoubleProperty getAmountProperty() {
		return this.amountProperty;
	}

	/**
	 * Gets the target currency property.
	 * 
	 * @return the targetCurrencyProperty
	 */
	public StringProperty getTargetCurrencyProperty() {
		return this.targetCurrencyProperty;
	}

	/**
	 * Gets the result property.
	 * 
	 * @return the resultProperty
	 */
	public StringProperty getResultProperty() {
		return this.resultProperty;
	}

	/**
	 * Gets the error message.
	 * 
	 * @return errorMessageProperty
	 */
	public StringProperty getErrorMessageProperty() {
		return this.errorMessageProperty;
	}

}
